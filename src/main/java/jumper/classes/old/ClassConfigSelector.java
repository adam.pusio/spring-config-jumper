package jumper.classes.old;

import static jumper.classes.old.Annotations.CONFIGURATION_PROPERTIES;
import static jumper.classes.old.Annotations.CONSTRUCTOR_BINDING;

import java.util.Collections;
import java.util.Optional;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import com.intellij.codeInsight.AnnotationUtil;
import com.intellij.openapi.components.Service;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ContentIterator;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiAnnotation;
import com.intellij.psi.PsiAnnotationMemberValue;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiJavaFile;
import com.intellij.psi.PsiManager;

import it.unimi.dsi.fastutil.Pair;

@Service
public final class ClassConfigSelector implements ContentIterator {

	private final Project project;

	public ClassConfigSelector(Project project) {
		this.project = project;
	}

	@Override
	public boolean processFile(@NotNull VirtualFile fileOrDir) {
		final ConfigClassProcessor configClassProcessor = project.getService(ConfigClassProcessor.class);
		getJavaFileFromVirtualFile(fileOrDir)
			.ifPresent(b -> configClassProcessor.process(b.left(), b.right()));
		return true;
	}

	@NotNull
	public Optional<Pair<String, PsiClass>> getJavaFileFromVirtualFile(VirtualFile fileOrDir) {
		if (fileOrDir == null) {
			return Optional.empty();
		}
		return Optional.ofNullable(PsiManager.getInstance(project).findFile(fileOrDir))
				   .filter(file -> file instanceof PsiJavaFile)
				   .map(PsiJavaFile.class::cast)
				   .map(this::processJavaFile)
				   .filter(Optional::isPresent)
				   .map(Optional::get);
	}

	private Optional<Pair<String, PsiClass>> processJavaFile(PsiJavaFile javaFile) {
		return Stream.of(javaFile.getClasses())
				   .filter(this::isMainBindingClass)
				   .findFirst()
				   .map(this::extractPropertyAndClass);
	}

	private Pair<String, PsiClass> extractPropertyAndClass(PsiClass psiClass) {
		return Stream.of(extractConfigPropertyAnnotation(psiClass))
				   .findFirst()
				   .map(annotation -> {
					   final String prefix = extractPrefixFromAnnotation(annotation);
					   return Pair.of(prefix, psiClass);
				   })
				   .orElseThrow(() -> new IllegalStateException("cannot process class that should be annotated with @ConfigurationProperties"));
	}

	private PsiAnnotation[] extractConfigPropertyAnnotation(PsiClass psiClass) {
		return AnnotationUtil.findAnnotations(psiClass, Collections.singletonList(CONFIGURATION_PROPERTIES));
	}

	private String extractPrefixFromAnnotation(com.intellij.psi.PsiAnnotation annotation) {
		final PsiAnnotationMemberValue prefixValue = annotation.findAttributeValue("prefix");
		final String prefix = StringUtils.strip(prefixValue.getText(), "\"");
		return prefix;
	}

	private boolean isMainBindingClass(PsiClass psiClass) {
		return isAnnotated(psiClass, CONSTRUCTOR_BINDING) && isAnnotated(psiClass, CONFIGURATION_PROPERTIES);
	}

	private boolean isAnnotated(PsiClass psiClass, final String annotation) {
		return AnnotationUtil.isAnnotated(psiClass, Collections.singletonList(annotation), AnnotationUtil.CHECK_TYPE);
	}
}
