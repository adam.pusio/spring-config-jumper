package jumper.classes.old;

import com.intellij.ide.highlighter.JavaFileType;
import com.intellij.openapi.components.Service;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ProjectFileIndex;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public final class ConfigClassInitializer {

	private final Project project;

	public void initializeClassElements() {
		final ClassConfigSelector processor = project.getService(ClassConfigSelector.class);

		ProjectFileIndex
			.SERVICE
			.getInstance(project)
			.iterateContent(processor, file -> file.getFileType() == JavaFileType.INSTANCE);
	}
}
