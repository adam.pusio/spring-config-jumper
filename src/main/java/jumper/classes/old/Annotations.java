package jumper.classes.old;

public final class Annotations {
	public static final String CONSTRUCTOR_BINDING = "org.springframework.boot.context.properties.ConstructorBinding";
	public static final String CONFIGURATION_PROPERTIES = "org.springframework.boot.context.properties.ConfigurationProperties";

}
