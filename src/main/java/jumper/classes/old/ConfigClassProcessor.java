package jumper.classes.old;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import com.intellij.openapi.components.Service;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.NlsSafe;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiField;
import com.intellij.psi.PsiManager;
import com.intellij.psi.PsiType;
import com.intellij.psi.util.PsiTypesUtil;
import com.intellij.psi.util.PsiUtil;

import jumper.properties.yaml.YamlElementsCollection;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public final class ConfigClassProcessor {

	private final Project project;

	public void process(@NlsSafe String prefix, PsiClass psiClass) {
		//TODO proper way to do this would be to dive into constructor and check assigment
		// but because is seems to be complicated lets assume that constructor parameters will be called same as fields
		final String clearAnnotationValue = StringUtils.strip(prefix, "\"");

		final ClassesElementsCollection classesElementsCollection = project.getService(ClassesElementsCollection.class);
		final YamlElementsCollection yamlElementsCollection = project.getService(YamlElementsCollection.class);
		yamlElementsCollection.getFieldNamesByPrefix(prefix)
			.map(fieldName -> findFieldIgnoringCase(psiClass, fieldName))
			.filter(Objects::nonNull)
			.forEach(field -> {
				final PsiType fieldType = extractFieldTypesFrom(field);
				final String propertyName = createPropertyName(clearAnnotationValue, field);
				final PsiClass fieldClass = PsiTypesUtil.getPsiClass(fieldType);
				log.info("found in class: {} - {}", propertyName, field);
				classesElementsCollection.put(propertyName, field);
				if (fieldClass != null && PsiManager.getInstance(project).isInProject(fieldClass) && !(fieldClass.isEnum())) {
					process(propertyName, fieldClass);
				}
			});
	}

	private PsiField findFieldIgnoringCase(PsiClass psiClass, String fieldName) {
		return List.of(psiClass.getFields())
				   .stream()
				   .filter(field -> field.getName().equalsIgnoreCase(fieldName))
				   .findFirst()
				   .orElse(null);
	}

	@NotNull
	private String createPropertyName(String text, PsiField field) {
		final String fieldName = field.getName();
		return text + "." + fieldName;
	}

	@NotNull
	private PsiType extractFieldTypesFrom(PsiField psiField) {
		return Optional.ofNullable(
			//IN case of generics, tested only on lists
			//extracting Type from generics: https://stackoverflow.com/questions/36441456/how-can-i-get-generic-type-of-psifield
			PsiUtil.extractIterableTypeParameter(psiField.getType(), true))
				   .orElse(psiField.getType());
	}
}
