package jumper.classes.old;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import com.intellij.openapi.components.Service;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiField;

import jumper.properties.yaml.KeyFormatter;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public final class ClassesElementsCollection {
	private final Project project;
	private Map<String, PsiField> content = new HashMap<>();

	public void put(final String key, final PsiField field) {
		final KeyFormatter keyFormatter = project.getService(KeyFormatter.class);
		final String escapedKey = keyFormatter.escapeClassKey(key);
		content.put(escapedKey, field);
	}

	public List<String> getByFiled(final PsiField field) {
		return content.entrySet()
				   .stream()
				   .filter(a -> a.getValue().equals(field))
				   .map(Map.Entry::getKey)
				   .collect(Collectors.toList());
	}

	public Optional<PsiField> getByKey(final String key) {
		final KeyFormatter keyFormatter = project.getService(KeyFormatter.class);
		final String escapedKey = keyFormatter.escapeClassKey(key);
		return Optional.ofNullable(content.get(escapedKey));
	}

	public void clear() {
		content.clear();
	}
}
