package jumper.caret;

import org.jetbrains.annotations.NotNull;

import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.components.Service;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;

@Service
public final class CaretService {

	public PsiElement getCarretElement(@NotNull AnActionEvent e) {
		final int caretOffset = getCaretOffset(e);
		final PsiFile psiFile = e.getData(CommonDataKeys.PSI_FILE);
		final PsiElement elementAt = psiFile.findElementAt(caretOffset);
		final PsiElement psiElement = elementAt.getParent();
		return psiElement;
	}

	private int getCaretOffset(AnActionEvent e) {
		return e.getRequiredData(CommonDataKeys.EDITOR)
				   .getCaretModel()
				   .getPrimaryCaret()
				   .getOffset();
	}
}
