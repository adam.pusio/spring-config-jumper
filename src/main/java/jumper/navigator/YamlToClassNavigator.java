package jumper.navigator;

import java.util.Optional;

import org.jetbrains.yaml.psi.YAMLPsiElement;

import com.intellij.codeInsight.navigation.NavigationUtil;
import com.intellij.openapi.components.Service;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiField;

import jumper.classes.old.ClassesElementsCollection;
import jumper.properties.yaml.KeyFormatter;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public final class YamlToClassNavigator {
	private static final Logger log = Logger.getInstance(YamlToClassNavigator.class);
	private final Project project;

	public void navigate(YAMLPsiElement yamlPsiElement) {
		final String key = findKeyFromCurrentProperty(yamlPsiElement);
		findClassByKey(key)
			.ifPresent(this::navigate);
	}

	private void navigate(PsiField field) {
		log.info("navigating to: " + field);
		NavigationUtil.activateFileWithPsiElement(field);
	}

	private Optional<PsiField> findClassByKey(String key) {
		log.info("searching for key: " + key);
		final ClassesElementsCollection classesCollection = project.getService(ClassesElementsCollection.class);
		return classesCollection.getByKey(key);
	}

	private String findKeyFromCurrentProperty(YAMLPsiElement currentProperty) {
		log.info("searching for property: " + currentProperty);
		return project.getService(KeyFormatter.class).escapeYamlKey(currentProperty);
	}
}
