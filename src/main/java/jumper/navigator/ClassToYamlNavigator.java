package jumper.navigator;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.jetbrains.annotations.NotNull;

import com.intellij.codeInsight.navigation.NavigationUtil;
import com.intellij.openapi.components.Service;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiField;

import jumper.classes.old.ClassesElementsCollection;
import jumper.properties.yaml.YamlElementsCollection;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public final class ClassToYamlNavigator {
	private static final Logger log = Logger.getInstance(ClassToYamlNavigator.class);
	private final Project project;

	public void navigate(PsiField currentField) {
		final List<PsiElement> yamlElements = findCorrespondingFields(currentField);
		navigate(yamlElements);
	}

	@NotNull
	private List<PsiElement> findCorrespondingFields(PsiField currentField) {
		return findKeyFromCurrentField(currentField)
				   .stream()
				   .map(this::findYamlPropertiesByKey)
				   .flatMap(Collection::stream)
				   .collect(Collectors.toList());
	}

	private void navigate(Collection<PsiElement> yamlPsiElements) {
		if (yamlPsiElements.isEmpty()) {
			return;
		}
		if (yamlPsiElements.size() == 1) {
			yamlPsiElements.stream().findFirst().ifPresent(elt -> {
				log.info("found corresponding yaml: " + elt + " and navigating to it");
				NavigationUtil.activateFileWithPsiElement(elt);
			});
		} else {
			log.info("has more than one yaml corresponding values, displaying popup");
			PsiElement[] foos = yamlPsiElements.toArray(new PsiElement[yamlPsiElements.size()]);
			NavigationUtil.getPsiElementPopup(foos, "Jump to Property")
				.showInBestPositionFor(FileEditorManager.getInstance(project).getSelectedTextEditor());
		}
	}

	private Collection<PsiElement> findYamlPropertiesByKey(String key) {
		log.info("searching for key: " + key);
		final YamlElementsCollection yamlCollection = project.getService(YamlElementsCollection.class);
		return yamlCollection.getByKey(key);
	}

	private List<String> findKeyFromCurrentField(PsiField currentField) {
		log.info("searching for field: " + currentField);
		final ClassesElementsCollection classesCollection = project.getService(ClassesElementsCollection.class);
		return classesCollection.getByFiled(currentField);
	}
}
