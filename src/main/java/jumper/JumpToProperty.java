package jumper;

import org.jetbrains.annotations.NotNull;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiField;

import jumper.caret.CaretService;
import jumper.classes.old.ConfigClassInitializer;
import jumper.navigator.ClassToYamlNavigator;
import jumper.properties.yaml.YamlInitializer;

public class JumpToProperty extends AnAction {
	@Override
	public void actionPerformed(@NotNull AnActionEvent e) {
		final Project eventProject = getEventProject(e);
		initializeCollections(eventProject);
		final PsiElement psiElement = getCaretElement(e);
		if (psiElement instanceof PsiField) {
			final PsiField psiField = (PsiField) psiElement;
			eventProject.getService(ClassToYamlNavigator.class)
				.navigate(psiField);
		}
	}

	private void initializeCollections(Project eventProject) {
		final YamlInitializer yamlInitializer = eventProject.getService(YamlInitializer.class);
		yamlInitializer.initializeYamlElements();
		final ConfigClassInitializer configClassInitializer = eventProject.getService(ConfigClassInitializer.class);
		configClassInitializer.initializeClassElements();
	}


	@Override
	public void update(@NotNull AnActionEvent e) {
		e.getPresentation().setEnabledAndVisible(false);
		final PsiElement psiElement = getCaretElement(e);
		if (psiElement instanceof PsiField) {
			e.getPresentation().setEnabledAndVisible(true);
		}
	}

	public PsiElement getCaretElement(@NotNull AnActionEvent e){
		return getEventProject(e)
				   .getService(CaretService.class)
				   .getCarretElement(e);
	}
}
