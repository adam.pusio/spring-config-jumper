package jumper;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.yaml.psi.YAMLPsiElement;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiElement;

import jumper.caret.CaretService;
import jumper.classes.old.ClassesElementsCollection;
import jumper.classes.old.ConfigClassInitializer;
import jumper.navigator.YamlToClassNavigator;
import jumper.properties.yaml.YamlElementsCollection;
import jumper.properties.yaml.YamlInitializer;

public class JumpToClass extends AnAction {
	@Override
	public void actionPerformed(@NotNull AnActionEvent e) {
		final Project eventProject = getEventProject(e);
		initializeCollections(eventProject);
		final PsiElement psiElement = getCaretElement(e);

		if (psiElement instanceof YAMLPsiElement) {
			final YAMLPsiElement yamlPsiElement = (YAMLPsiElement) psiElement;
			eventProject.getService(YamlToClassNavigator.class)
				.navigate(yamlPsiElement);
		}
		clearCollections(eventProject);
	}

	private void initializeCollections(Project eventProject) {
		final YamlInitializer yamlInitializer = eventProject.getService(YamlInitializer.class);
		yamlInitializer.initializeYamlElements();
		final ConfigClassInitializer configClassInitializer = eventProject.getService(ConfigClassInitializer.class);
		configClassInitializer.initializeClassElements();
	}

	private void clearCollections(Project eventProject) {
		eventProject
			.getService(YamlElementsCollection.class)
			.clear();
		eventProject
			.getService(ClassesElementsCollection.class)
			.clear();
	}

	@Override
	public void update(@NotNull AnActionEvent e) {
		e.getPresentation().setEnabledAndVisible(false);
		final PsiElement psiElement = getCaretElement(e);
		if (psiElement instanceof YAMLPsiElement) {
			e.getPresentation().setEnabledAndVisible(true);
		}
	}

	public PsiElement getCaretElement(@NotNull AnActionEvent e) {
		return getEventProject(e)
				   .getService(CaretService.class)
				   .getCarretElement(e);
	}
}
