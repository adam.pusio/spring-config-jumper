package jumper.properties.yaml;

import org.apache.commons.lang3.RegExUtils;
import org.jetbrains.yaml.YAMLUtil;
import org.jetbrains.yaml.psi.YAMLPsiElement;

import com.intellij.openapi.components.Service;

@Service
public final class KeyFormatter {

	public String escapeYamlKey(YAMLPsiElement element) {
		final String key = YAMLUtil.getConfigFullName(element);
		return escapeClassKey(key);
	}

	public String escapeClassKey(final String key) {
		final String removedArrays = RegExUtils.removeAll(key, "\\[.*?\\]");
		final String removedNonWords = RegExUtils.removeAll(removedArrays, "[^\\w.]");
		return removedNonWords.toLowerCase();
	}
}
