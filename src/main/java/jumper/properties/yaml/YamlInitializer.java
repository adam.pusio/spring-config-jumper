package jumper.properties.yaml;

import com.intellij.openapi.components.Service;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ProjectFileIndex;

@Service
public final class YamlInitializer {
	private final Project project;

	public YamlInitializer(Project project) {
		this.project = project;
	}

	public void initializeYamlElements() {
		ProjectFileIndex
			.SERVICE
			.getInstance(project)
			.iterateContent(project.getService(YamlProcessor.class));
	}
}
