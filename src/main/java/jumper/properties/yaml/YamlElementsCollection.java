package jumper.properties.yaml;

import java.util.Collection;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.intellij.openapi.components.Service;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiElement;

@Service
public final class YamlElementsCollection {
	private final Multimap<String, PsiElement> content;
	private final Project project;

	public YamlElementsCollection(Project project) {
		this.project = project;
		this.content = HashMultimap.create();
	}

	public void put(final String key, final PsiElement yamlPsiElement) {
		final String escapedKey = escapeKey(key);
		content.put(escapedKey, yamlPsiElement);
	}

	public Collection<PsiElement> getByKey(final String key) {
		final String escapedKey = escapeKey(key);
		return content.get(escapedKey);
	}

	private String escapeKey(String key) {
		final KeyFormatter keyFormatter = project.getService(KeyFormatter.class);
		return keyFormatter.escapeClassKey(key);
	}

	public Stream<String> getFieldNamesByPrefix(final String prefix) {
		final String escapedKey = escapeKey(prefix);
		return content.keySet()
				   .stream()
				   .filter(key -> key.startsWith(escapedKey))
				   .map(key -> stripPrefixAndExtractField(key, escapedKey));
	}

	private String stripPrefixAndExtractField(final String key, final String prefix) {
		final String prefixRemoved = StringUtils.removeStart(key, prefix);
		final String dotRemoved = StringUtils.removeStart(prefixRemoved, ".");
		return StringUtils.substringBefore(dotRemoved, ".");
	}

	public void clear() {
		content.clear();
	}
}
