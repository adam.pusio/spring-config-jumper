package jumper.properties.yaml;

import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.yaml.YAMLFileType;
import org.jetbrains.yaml.psi.YAMLFile;
import org.jetbrains.yaml.psi.YAMLKeyValue;
import org.jetbrains.yaml.psi.YAMLPsiElement;
import org.jetbrains.yaml.psi.YAMLSequence;

import com.intellij.openapi.components.Service;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ContentIterator;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiManager;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public final class YamlProcessor implements ContentIterator {
	private final static Logger log = Logger.getInstance(YamlProcessor.class);
	private final Project project;

	@Override
	public boolean processFile(@NotNull VirtualFile fileOrDir) {
		//TODO application file name can be changed by spring property
		if (fileOrDir.getFileType() instanceof YAMLFileType && fileOrDir.getName().startsWith("application")) {
			final YamlElementsCollection collection = project.getService(YamlElementsCollection.class);

			final YAMLFile yamlFile = (YAMLFile) PsiManager.getInstance(project).findFile(fileOrDir);
			collectYamlData(yamlFile.getYAMLElements(), collection);
		}
		return true;
	}

	private void collectYamlData(List<YAMLPsiElement> yamlElements, YamlElementsCollection collection) {
		yamlElements.forEach(element -> {
			final String key = project.getService(KeyFormatter.class).escapeYamlKey(element);
			log.info("found in yaml file: " + key + " - " + element.getNavigationElement());
			if (element instanceof YAMLKeyValue) {
				collection.put(key, element.getNavigationElement());
			} else if (element instanceof YAMLSequence) {
				collection.put(key, element.getParent());
			}
			collectYamlData(element.getYAMLElements(), collection);
		});
	}
}