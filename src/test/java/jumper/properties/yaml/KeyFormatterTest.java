package jumper.properties.yaml;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class KeyFormatterTest {

	private final KeyFormatter keyFormatter = new KeyFormatter();

	@Test
	public void keyWithSemicolonTest() {
		//given
		final String text = "with-semicolon.something";

		//when
		final String result = keyFormatter.escapeClassKey(text);

		//then
		assertEquals("withsemicolon.something", result);
	}

	@Test
	public void keyWithFloorAndBracketTest() {
		//given
		final String text = "with-semicolon[22].something[0]";

		//when
		final String result = keyFormatter.escapeClassKey(text);

		//then
		assertEquals("withsemicolon.something", result);
	}

	@Test
	public void keyNaturalStringTest() {
		//given
		final String text = "withsemicolon.something";

		//when
		final String result = keyFormatter.escapeClassKey(text);

		//then
		assertEquals("withsemicolon.something", result);
	}
}